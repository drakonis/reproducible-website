---
layout: report
year: "2020"
month: "04"
title: "Reproducible Builds in April 2020"
draft: false
published: 2020-05-06 15:11:15
---

**Welcome to the April 2020 report from the [Reproducible Builds]({{ "/" | relative_url }}) project.** In our regular reports we outline the most important things that we and the rest of the community have been up to over the past month.
{: .lead}

[![]({{ "/images/reports/2020-04/reproducible-builds.png#right" | relative_url }})]({{ "/" | relative_url }})

*What are reproducible builds?* One of the original promises of open source software is that distributed peer review and transparency of process results in enhanced end-user security. But whilst anyone may inspect the source code of free and open source software for malicious flaws, almost all software today is distributed as pre-compiled binaries. This allows nefarious third-parties to compromise systems by injecting malicious code into seemingly secure software during the various compilation and distribution processes.

## News

[![]({{ "/images/reports/2020-04/rubygems.png#right" | relative_url }})](https://arstechnica.com/information-technology/2020/04/725-bitcoin-stealing-apps-snuck-into-ruby-repository/)

It was discovered that more than 725 malicious packages were downloaded thousands of times from [RubyGems](https://rubygems.org/), the official channel for distributing code for the Ruby programming language. Attackers used a variation of "[typosquatting](https://en.wikipedia.org/wiki/Typosquatting)" and replaced hyphens and underscores (for example, uploading a malevolent `atlas-client` in place of `atlas_client`) that [executed a script that intercepted Bitcoin payments](https://blog.reversinglabs.com/blog/mining-for-malicious-ruby-gems). ([Ars Technica report](https://arstechnica.com/information-technology/2020/04/725-bitcoin-stealing-apps-snuck-into-ruby-repository/))

Bernhard M. Wiedemann launched [`ismypackagereproducibleyet.org`](https://ismypackagereproducibleyet.org/), a service that takes a package name as input and displays whether the package is reproducible in a number of distributions. For example, it can quickly [show the status of Perl](https://ismypackagereproducibleyet.org/?pkg=perl) as being reproducible on [openSUSE](https://www.opensuse.org/) but not in [Debian](https://debian.org/).  Bernhard also improved the documentation of his ["unreproducible package"](https://github.com/bmwiedemann/theunreproduciblepackage) to add some example patches for hash issues.&nbsp;[[...](https://github.com/bmwiedemann/theunreproduciblepackage/commit/53d4263b461b7b7f1239e34536eaf77e5c61b174)].

[![]({{ "/images/reports/2020-04/ccc-post.png#right" | relative_url }})](https://www.ccc.de/en/updates/2020/contact-tracing-requirements)

There was a post on [Chaos Computer Club](https://www.ccc.de/en/)'s website listing  [*Ten requirements for the evaluation of "Contact Tracing" apps*](https://www.ccc.de/en/updates/2020/contact-tracing-requirements) in relation to the SARS-CoV-2 epidemic. In particular:

> **4. Transparency and verifiability:** The complete source code for the app and infrastructure must be freely available without access restrictions to allow audits by all interested parties. Reproducible build techniques must be used to ensure that users can verify that the app they download has been built from the audited source code.

Elsewhere, Nicolas Boulenguez [wrote a patch](https://gcc.gnu.org/bugzilla/show_bug.cgi?id=87972) for the [Ada programming language](https://en.wikipedia.org/wiki/Ada_(programming_language)) component of the [GCC compiler](https://gcc.gnu.org/) to skip `-f.*-prefix-map` options when writing [Ada Library Information](https://gcc.gnu.org/onlinedocs/gcc-9.3.0/gnat_ugn/The-Ada-Library-Information-Files.html) files. Amongst other properties, these `.ali` files embed the compiler flags used at the time of the build which results in the absolute build path being recorded via [`-ffile-prefix-map`](https://gcc.gnu.org/onlinedocs/gcc/Overall-Options.html#index-ffile-prefix-map), `-fdebug-prefix-map`, etc.

[![]({{ "/images/reports/2020-04/archlinux.png#right" | relative_url }})](https://archlinux.org)

In the [Arch Linux](https://archlinux.org/) project, *kpcyrd* reported that [they held their first "rebuilder workshop"](https://lists.reproducible-builds.org/pipermail/rb-general/2020-April/001892.html). The session was held on IRC and participants were provided a document with instructions on how to install and use Arch's [`repro`](https://github.com/archlinux/archlinux-repro/) tool. The meeting resulted in multiple people with no prior experience of Reproducible Builds validate their first package. Later in the month he also announced that it was [now possible to run independent rebuilders](https://lists.reproducible-builds.org/pipermail/rb-general/2020-April/001905.html) under Arch in a "hands-off, everything just works™" solution to distributed package verification.

[Mathias Lang](https://twitter.com/Geod241) submitted a pull request against [`dmd`](https://dlang.org/dmd-linux.html), the canonical compiler for the ['D' programming language](https://dlang.org/)to add support for our [`SOURCE_DATE_EPOCH`](https://reproducible-builds.org/specs/source-date-epoch/) environment variable as well the other C preprocessor tokens such `__DATE__`, `__TIME__` and `__TIMESTAMP__` which was subsequently merged. `SOURCE_DATE_EPOCH` defines a distribution-agnostic standard for build toolchains to consume and emit timestamps in situations where they are deemed to be necessary.&nbsp;[[...](https://github.com/dlang/dmd/pull/11035)]

[![]({{ "/images/reports/2020-04/telegram.png#right" | relative_url }})](https://telegram.org)

The [Telegram](https://telegram.org/) instant-messaging platform [announced that they had updated to version 5.1.1](https://twitter.com/TelegramBeta/status/1256210359570046976) continuing their claim that they are reproducible according to [their full instructions](https://core.telegram.org/reproducible-builds) and therefore verifying that its original source code is exactly the same code that is used to build the versions available on the Apple App Store and Google Play distribution platforms respectfully.

Lastly, Hervé Boutemy reported that 97% of the [current development versions of various Maven packages](https://github.com/jvm-repo-rebuild/reproducible-maven-HEAD) appear to have a reproducible build.&nbsp;[[...](https://lists.reproducible-builds.org/pipermail/rb-general/2020-April/001882.html)]

<br>

## Distribution work

[![]({{ "/images/reports/2020-04/debian.png#right" | relative_url }})](https://debian.org/)


In [Debian](https://debian.org/) this month, 89 reviews of Debian packages were added, 21 were updated and 33 were removed this month adding to [our knowledge about identified issues](https://tests.reproducible-builds.org/debian/index_issues.html). Many issue types were noticed, categorised and updated by Chris Lamb, including:

* [`captures_build_path_in_hd5_database_files`](https://tests.reproducible-builds.org/debian/issues/unstable/captures_build_path_in_hd5_database_files.html)
* [`cargo_installs_crates2_json`](https://tests.reproducible-builds.org/debian/issues/unstable/cargo_installs_crates2_json.html)
* [`nondeterministic_devhelp_documentation_generated_by_gtk_doc`](https://tests.reproducible-builds.org/debian/issues/unstable/nondeterministic_devhelp_documentation_generated_by_gtk_doc.html)
* [`ros_dynamic_reconfigure_captures_build_path`](https://tests.reproducible-builds.org/debian/issues/unstable/ros_dynamic_reconfigure_captures_build_path.html)

In addition, Holger Levsen filed a feature request against [`debrebuild`](https://salsa.debian.org/debian/devscripts/-/blob/master/scripts/debrebuild.pl), a tool for rebuilding a Debian package given a `.buildinfo` file, proposing to [add `--standalone` or `--one-shot-mode`](https://bugs.debian.org/958750) functionality.

<br>

[![]({{ "/images/reports/2020-04/opensuse.png#right" | relative_url }})](https://www.opensuse.org/)

In [openSUSE](https://www.opensuse.org/), Bernhard M. Wiedemann made the following changes:

* [`blender`](https://build.opensuse.org/request/show/791039) (sort C `readdir` call, [rejected upstream](https://developer.blender.org/D5858))
* [`guile/guix`](https://bugzilla.opensuse.org/show_bug.cgi?id=1170378) (parallelism race condition)
* [`mingw32-filesystem/mingw32-binutils`](https://build.opensuse.org/request/show/795715) (sort `readdir`, filesystem, toolchain)
* [`mingw64-filesystem/mingw64-binutils`](https://build.opensuse.org/request/show/795584) (sort `readdir`, filesystem, toolchain)
* [`musescore`](https://build.opensuse.org/request/show/798383) (non-deterministic `.zip` files)
* [`OBS`](https://bugzilla.opensuse.org/show_bug.cgi?id=1170524) (FTBFS in rebuild)
* [`perl-Image-Sane`](https://bugzilla.opensuse.org/show_bug.cgi?id=1170639) (report hung build on a single core VM)
* [`ruby2.7`](https://build.opensuse.org/request/show/793752) (date, [already upstream](https://github.com/ruby/io-console/commit/679a941d05d869f5e575730f6581c027203b7b26))
* [`vtk`](https://build.opensuse.org/request/show/798062) (drop unreproducible `.pyc` file)

[![]({{ "/images/reports/2020-04/archlinux.png#right" | relative_url }})](https://archlinux.org)

In [Arch Linux](https://archlinux.org), a rebuilder instance has been setup at [reproducible.archlinux.org](http://reproducible.archlinux.org/) that is rebuilding Arch's `[core]` repository directly. The first rebuild has led to approximately 90% packages reproducible contrasting with 94% on the Reproducible Build's project own [ArchLinux status page on `tests.reproducible-builds.org`](https://tests.reproducible-builds.org/archlinux/state_core_reproducible.html) that continiously builds packages and does not verify Arch Linux packages. More information may be found on the [corresponding wiki page](https://wiki.archlinux.org/index.php/Package_rebuilders) and the [underlying decisions were explained](https://lists.reproducible-builds.org/pipermail/rb-general/2020-April/001892.html) on [our mailing list](https://lists.reproducible-builds.org/listinfo/rb-general/).

<br>

## Software development

#### [diffoscope](https://diffoscope.org)

[![]({{ "/images/reports/2020-04/diffoscope.png#right" | relative_url }})](https://diffoscope.org)

Chris Lamb made the following changes to [diffoscope](https://diffoscope.org), the Reproducible Builds project's in-depth and content-aware diff utility that can locate and diagnose reproducibility issues (including preparing and uploading versions `139`, `140`, `141`, `142` and `143` to Debian which were subsequently uploaded to the [*backports*](https://backports.debian.org/) repository):

* Comparison improvements:

    * [Dalvik](https://source.android.com/devices/tech/dalvik) `.dex` files can also serve as [APK containers](https://en.wikipedia.org/wiki/Android_application_package) so restrict the narrower identification of `.dex` files to files ending with this extension and widen the identification of APK files to when file(1) discovers a Dalvik file.&nbsp;([#28](https://salsa.debian.org/reproducible-builds/diffoscope/issue/28))
    * Add support for Hierarchical Data Format (HD5) files.&nbsp;([#95](https://salsa.debian.org/reproducible-builds/diffoscope/issues/95))
    * Add support for `.p7c` and `.p7b` certificates.&nbsp;([#94](https://salsa.debian.org/reproducible-builds/diffoscope/issues/94))
    * Strip paths from the output of `zipinfo(1)` warnings.&nbsp;([#97](https://salsa.debian.org/reproducible-builds/diffoscope/issues/97))
    * Don't uselessly include the JSON "similarity" percentage if it is "0.0%".&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/f68a087)]
    * Render multi-line difference comments in a way to show indentation.&nbsp;([#101](https://salsa.debian.org/reproducible-builds/diffoscope/commit/b72ff87))

* Testsuite improvements:

    * Add `pdftotext` as a requirement to run the PDF `test_metadata` text.&nbsp;([#99](https://salsa.debian.org/reproducible-builds/diffoscope/commit/c2a35f9))
    * [apktool](https://ibotpeaches.github.io/Apktool/) 2.5.0 changed the handling of output of [XML schemas](https://en.wikipedia.org/wiki/XML_schema) so update and restrict the corresponding test to match.&nbsp;([#96](https://salsa.debian.org/reproducible-builds/diffoscope/issues/96))
    * Explicitly list `python3-h5py` in `debian/tests/control.in` to ensure that we have this module installed during a test run to generate the fixtures in these tests.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/451b8ab)]
    * Correct parsing of `./setup.py test --pytest-args` arguments.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/2fb1ac2)]

* Misc:

    * Capitalise "Ordering differences only" in text comparison comments.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/30be510)]
    * Improve documentation of `FILE_TYPE_HEADER_PREFIX` and `FALLBACK_FILE_TYPE_HEADER_PREFIX` to highlight that only the first 16 bytes are used.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/5a8d64f)]

Michael Osipov created a well-researched merge request to return *diffoscope* to using `zipinfo` directly instead of piping input via `/dev/stdin` in order to ensure portability to the [BSD operating system](https://en.wikipedia.org/wiki/Berkeley_Software_Distribution)&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/125b140)]. In addition, [Ben Hutchings](https://www.decadent.org.uk/ben/) documented how `--exclude` arguments are matched against filenames&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/137111b)] and Jelle van der Waa updated the [LLVM](https://llvm.org/) test fixture difference for LLVM version 10&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/c645b97)] as well as adding a reference to the name of the `h5dump` tool in [Arch Linux](https://archlinux.org/)&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/86bbbec)].

Lastly, Mattia Rizzolo also fixed in incorrect build dependency&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/beb845d)] and Vagrant Cascadian enabled *diffoscope* to locate the `openssl` and `h5dump` packages on [GNU Guix](https://guix.gnu.org/)&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/108bcb7)][[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/e80650b)], and updated diffoscope in [GNU Guix](https://guix.gnu.org/) to version 141&nbsp;[[...](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=2d9886f7e8838d6aeb0cfb20a2a49fc7d8fb233c)] and 143&nbsp;[[...](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=ac4fefb2983aff5e5972b4279aee5296a08aab6a)].

#### [strip-nondeterminism](https://tracker.debian.org/pkg/strip-nondeterminism)

[strip-nondeterminism](https://tracker.debian.org/pkg/strip-nondeterminism) is our tool to remove specific non-deterministic results from a completed build. In April, Chris Lamb made the following changes:

* Add deprecation plans to all handlers documenting how — or if — they could be disabled and eventually removed, etc.&nbsp;([#3](https://salsa.debian.org/reproducible-builds/strip-nondeterminism/commit/c0d6b21))
* Normalise `*.sym` files as Java archives.&nbsp;([#15](https://salsa.debian.org/reproducible-builds/strip-nondeterminism/issue/15))
* Add support for custom `.zip` filename filtering and exclude two patterns of files generated by [Maven](http://maven.apache.org/) projects in "fork" mode.&nbsp;([#13](https://salsa.debian.org/reproducible-builds/strip-nondeterminism/issue/13))

#### [disorderfs](https://tracker.debian.org/pkg/disorderfs)

[disorderfs](https://tracker.debian.org/pkg/disorderfs) is our [FUSE](https://en.wikipedia.org/wiki/Filesystem_in_Userspace)-based filesystem that deliberately introduces non-determinism into directory system calls in order to flush out reproducibility issues.

This month, Chris Lamb fixed a long-standing issue by not drop UNIX groups in FUSE multi-user mode when we are not root&nbsp;([#1](https://salsa.debian.org/reproducible-builds/disorderfs/issues/1)) and uploaded version `0.5.9-1` to Debian *unstable*. Vagrant Cascadian subsequently refreshed disorderfs in [GNU Guix](https://guix.gnu.org/) to version 0.5.9&nbsp;[[...](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=cdac010c1936f5d909d4b7f74961fa41ad754f3e)].

#### Upstream patches

The Reproducible Builds project detects, dissects and attempts to fix as many currently-unreproducible packages as possible. We endeavour to send all of our patches upstream where appropriate. This month, we wrote a large number of such patches, including:

* Bernhard M. Wiedemann:

    * [`elixir`](https://github.com/elixir-lang/elixir/issues/10000) (parallelism)
    * [`gnutls`](https://gitlab.com/gnutls/gnutls/-/merge_requests/1230) (build failure)
    * [`moonjit/bcc`](https://github.com/moonjit/moonjit/issues/110) (compile-time CPU-detection)
    * [`openstack`](https://review.opendev.org/#/c/717164) (backport of patch to drop unreproducible sphinx `.pickle` files)
    * [`x3270`](https://sourceforge.net/p/x3270/code/merge-requests/2/) (merged, update date patch)

* Chris Lamb:

    * [#958301](https://bugs.debian.org/958301) filed against [`dh-cargo`](https://tracker.debian.org/pkg/dh-cargo).
    * [#956549](https://bugs.debian.org/956549) filed against [`gmap`](https://tracker.debian.org/pkg/gmap).
    * [#956591](https://bugs.debian.org/956591) filed against [`gpick`](https://tracker.debian.org/pkg/gpick).
    * [#956477](https://bugs.debian.org/956477) filed against [`herbstluftwm`](https://tracker.debian.org/pkg/herbstluftwm).
    * [#956304](https://bugs.debian.org/956304) filed against [`libcamera`](https://tracker.debian.org/pkg/libcamera).
    * [#956589](https://bugs.debian.org/956589) filed against [`libctl`](https://tracker.debian.org/pkg/libctl).
    * [#956408](https://bugs.debian.org/956408) filed against [`minetest-mod-xdecor`](https://tracker.debian.org/pkg/minetest-mod-xdecor).
    * [#955783](https://bugs.debian.org/955783) filed against [`netgen-lvs`](https://tracker.debian.org/pkg/netgen-lvs).
    * [#958110](https://bugs.debian.org/958110) filed against [`nickle`](https://tracker.debian.org/pkg/nickle).
    * [#958381](https://bugs.debian.org/958381) filed against [`nmrpflash`](https://tracker.debian.org/pkg/nmrpflash).
    * [#958382](https://bugs.debian.org/958382) filed against [`node-mqtt`](https://tracker.debian.org/pkg/node-mqtt).
    * [#956473](https://bugs.debian.org/956473) filed against [`sprai`](https://tracker.debian.org/pkg/sprai).
    * [#955501](https://bugs.debian.org/955501) filed against [`yaz`](https://tracker.debian.org/pkg/yaz).
    * [#956583](https://bugs.debian.org/956583) filed against [`xxhash`](https://tracker.debian.org/pkg/xxhash).

In addition, Bernhard informed the following projects that their packages are not reproducible:

* [`acoular`](https://github.com/acoular/acoular/issues/36) (report unknown non-determinism)
* [`cri-o`](https://github.com/cri-o/cri-o/issues/3702) (report a date issue)
* [`gnutls`](https://gitlab.com/gnutls/gnutls/-/issues/971) (report `certtool` being unable to extend certificates beyond 2049)
* [`gnutls`](https://gitlab.com/gnutls/gnutls/-/issues/980) (report copyright year variation)
* [`libxslt`](https://gitlab.gnome.org/GNOME/libxslt/-/issues/37) (report a bug about non-deterministic output from data corruption)
* [`python-astropy`](https://github.com/astropy/astropy/issues/10228) (report a future build failure in 2021)

#### Project documentation

[![]({{ "/images/reports/2020-04/website.png#right" | relative_url }})]({{ "/" | relative_url }})

This month, Chris Lamb made a large number of changes to [our website and documentation](https://reproducible-builds.org/) in the following categories:

* Community engagement improvements:

    * Update instructions to register for Salsa on our [Contribute](https://reproducible-builds.org/contribute/) page now that the signup process has been overhauled.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/7ba4ae1)]
    * Make it clearer that joining the [`rb-general`](https://lists.reproducible-builds.org/listinfo/rb-general) mailing list is probably a first step for contributors to take.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/cbb4ce0)]
    * Make our full contact information easier to find in the footer ([#19](https://salsa.debian.org/reproducible-builds/reproducible-website/issues/19)) and improve text layout using bullets to separate sections [[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/e9da17c)].

* Accessibility:

    * To improve accessibility, make all links underlined.&nbsp;([#12](https://salsa.debian.org/reproducible-builds/reproducible-website/issues/12))
    * Use an enhanced foreground/background contrast ratio of 7.04:1.&nbsp;([#11](https://salsa.debian.org/reproducible-builds/reproducible-website/issues/11))

* General improvements:

    * Add a new [Academic publications](https://reproducible-builds.org/docs/publications/) page.&nbsp;([#22](https://salsa.debian.org/reproducible-builds/reproducible-website/issues/22))
    * Add [Trezor](https://trezor.io/) to our [list of affiliated projects](https://reproducible-builds.org/who/).&nbsp;([#26](https://salsa.debian.org/reproducible-builds/reproducible-website/issues/26))
    * Add the [JVM](https://reproducible-builds.org/docs/jvm/) page to the [documentation index](https://reproducible-builds.org/docs/) ([#17](https://salsa.debian.org/reproducible-builds/reproducible-website/issues/17)) and tidy the page itself a little [[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/c93eb81)].
    * Add a [GNU Libtool](https://www.gnu.org/software/libtool/) pointer to the [Archive metadata](https://reproducible-builds.org/docs/archives/) documentation page.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/8e01958)]

* Internals:
    * Move to using [`jekyll-redirect-from`](https://github.com/jekyll/jekyll-redirect-from) over manual redirect pages [[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/1ab8fa4)][[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/b3e79ff)] and add a redirect from `/docs/buildinfo/` to `/docs/recording/`.&nbsp;([#23](https://salsa.debian.org/reproducible-builds/reproducible-website/issues/23))
    * Limit the website self-check to not scan generated files [[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/e5e8424)] and remove the "old layout" checker now that I have migrated all them [[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/d9e24a9)].
    * Move the news archive under the `/news/` namespace [[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/c7010eb)] and improve formatting of archived news links [[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/49dacb3)].
    * Various improvements to the draft template generation.&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/d31bafc)][[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/2ac054e)][[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/dba77a7)][[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/604af57)]

In addition, Holger Levsen clarified exactly which month we ceased to do weekly reports&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/3c08673)] and Mattia Rizzolo adjusted the title style of an event page&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/156aa34)].

Marcus Hoffman also started a discussion on our website's issue tracker asking for [clarification on embedded signatures](https://salsa.debian.org/reproducible-builds/reproducible-website/-/issues/25) and Chris Lamb [subsequently replied and asked Marcus to go ahead](https://salsa.debian.org/reproducible-builds/reproducible-website/-/issues/25#note_157047) and propose a concrete change.

#### Testing framework

[![]({{ "/images/reports/2020-04/testframework.png#right" | relative_url }})](https://tests.reproducible-builds.org/)

We operate a large and many-featured [Jenkins](https://jenkins.io/)-based testing framework that powers [`tests.reproducible-builds.org`](https://tests.reproducible-builds.org) that, amongst many other tasks, tracks the status of our reproducibility efforts as well as identifies any regressions that have been introduced.

* Chris Lamb:

    * Print the build environment prior to executing a build.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/f48b8542)]
    * Drop a misleading `disorderfs-debug` prefix in log output when we change non-disorderfs things in the file and, as it happens, do not run disorderfs at all.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/4d81837b)]
    * The CSS for the package report pages added a margin to all `<a>` HTML elements under `<li>` ones, which was causing a comma/bullet spacing issue.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/2220bac8)]
    * Tidy the copy in the project links sidebar.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/011ada16)]

* Holger Levsen:

    * General:
        * Install [`jekyll-redirect-from`](https://github.com/jekyll/jekyll-redirect-from) as it now needed by the [reproducible-builds.org]({{ "/" | relative_url }}) website.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/2e545d4a)]
        * Improve/correct log parsing rules.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/c372890b)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/99e3da67)]

    * Debian:

        * Reduce scheduling frequency of the *buster* distribution on the `arm64` architecture, etc..&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/6d9be0f0)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/279054ab)]
        * Show builds per day on a per-architecture basis for the last year on the Debian dashboard.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/28596ac5)]
        * Drop the [Subgraph OS](https://subgraph.com/sgos/) package set as development halted in 2017 or 2018.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/484fcd47)]
        * Update `debrebuild` to version from the latest version of `devscripts`.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/31f3d3c5)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/08b0c032)]
        * Add or improve various parts of the documentation.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/36c30638)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/2ae360dd)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/52639584)]

    * Work on a Debian rebuilder:

        * Integrate `sbuild`.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/73491c82)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/f2a7c8fa)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/e60191e9)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/b654e217)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/927c1294)]
        * Select a random `.buildinfo` file and attempt to build and compare the result.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/93287f6d)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/fbb85afb)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/11860066)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/4daa1127)]
        * Improve output and related output formatting.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/b8ddff93)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/120d4d5d)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/b840ae4c)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/16a76140)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/010c12ee)]
        * Outline next steps for the development of the tool.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/9c148545)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/cac53f4b)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/bc4dd1d9)]
        * Various refactoring and code improvements.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/343c9883)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/ab3de238)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/f4733b9f)]

Lastly, Mattia Rizzolo fixed some log parsing code regarding potentially-harmless warnings from package installation&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/df904c04)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/5201fb86)] and the usual build node maintenance was performed by Holger Levsen&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/4b51e82d)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/fcd3fcfb)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/531448ab)] and Mattia Rizzolo&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/72bb2afd)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/ee643fbb)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/16720116)].

---

## Misc news

On [our mailing list](https://lists.reproducible-builds.org/listinfo/rb-general/) this month, Santiago Torres asked [whether we were still publishing releases of our tools](https://lists.reproducible-builds.org/pipermail/rb-general/2020-April/001886.html) to our website and Chris Lamb replied that [this was not the case and fixed the issue](https://lists.reproducible-builds.org/pipermail/rb-general/2020-April/001887.html). Later in the month Santiago also reported that [the signature for the `disorderfs` package did not pass its GPG verification](https://lists.reproducible-builds.org/pipermail/rb-general/2020-April/001901.html) which was also fixed by Chris Lamb.

Hans-Christoph Steiner of the [Guardian Project](https://guardianproject.info/) asked whether there would be interest in [making our website translatable](https://lists.reproducible-builds.org/pipermail/rb-general/2020-April/001893.html) which resulted in [a WIP merge request](https://salsa.debian.org/reproducible-builds/reproducible-website/-/merge_requests/56) being filed against the website and a discussion on [how to track translation updates](https://lists.reproducible-builds.org/pipermail/rb-general/2020-April/001897.html).

<br>

If you are interested in contributing to the Reproducible Builds project, please visit our [*Contribute*]({{ "/contribute/" | relative_url }}) page on our website. However, you can get in touch with us via:

[![]({{ "/images/reports/2020-04/reproducible-builds.png#right" | relative_url }})]({{ "/" | relative_url }})

 * IRC: `#reproducible-builds` on `irc.oftc.net`.

 * Twitter: [@ReproBuilds](https://twitter.com/ReproBuilds) &bull; [@reproducible_builds@fosstodon.org](https://fosstodon.org/@reproducible_builds)

 * Reddit: [/r/ReproducibleBuilds](https://reddit.com/r/reproduciblebuilds)

 * Mailing list: [`rb-general@lists.reproducible-builds.org`](https://lists.reproducible-builds.org/listinfo/rb-general)

<br>

This month's report was written by Bernhard M. Wiedemann, Chris Lamb, Daniel Shahaf, Holger Levsen, Jelle van der Waa, *kpcyrd*, Mattia Rizzolo and Vagrant Cascadian. It was subsequently reviewed by a bunch of Reproducible Builds folks on IRC and the mailing list.
{: .small}
